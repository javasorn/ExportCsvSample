﻿using ExportCsvSample.Entity;
using ExportCsvSample.Logic;
using ExportCsvSample.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExportCsvSample.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        [Route("Download")]
        [HttpGet]
        public async Task<IActionResult> Download()
        {
            var timeout = 180;
            var contextOptions = new DbContextOptionsBuilder<DatabaseContext>()
                .UseSqlServer(
                    "Initial Catalog = MyApp; Data Source =.; Integrated Security = True; Pooling = true",
                    options =>
                    {
                        if (timeout > 0)
                        {
                            options.CommandTimeout(timeout);
                        }
                    }
                )
                .Options;

            using (var context = new DatabaseContext(contextOptions))
            {
                var query = "select * from Employees";
                var reader = CsvCreator.RawSqlQuery(context,query);
                var result = CsvCreator.GetCsvFromQueryAsString(reader);
                return new FileContentResult(result.SelectMany(s => Encoding.UTF8.GetBytes(s)).ToArray(), "text/csv")
                {
                    FileDownloadName = "export.csv",
                };
            }
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
