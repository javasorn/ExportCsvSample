﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace ExportCsvSample.Logic
{
    public static class CsvCreator
    {
        public static IDataReader RawSqlQuery(DbContext context, string query)
        {
            using (var command = context.Database.GetDbConnection().CreateCommand())
            {
                command.CommandText = query;
                command.CommandType = CommandType.Text;
                context.Database.OpenConnection();
                return command.ExecuteReader();                
            }
        }

        public static IEnumerable<string> GetCsvFromQueryAsString(IDataReader reader)
        {
            using (reader)
            {
                string[] columnNames = new string[reader.FieldCount];

                for (int i = 0; i < columnNames.Length; i++)
                {
                    columnNames[i] = reader.GetName(i);
                }

                yield return string.Join(",", columnNames);
                yield return "\r\n";

                object[] values = new object[reader.FieldCount];

                while (reader.Read())
                {
                    reader.GetValues(values);

                    string commaSeparatedValues = string.Join(",", values.Select(o => o != null ? EscapeCsv(o.ToString()) : "").ToArray());
                    yield return commaSeparatedValues;
                    yield return "\r\n";
                }
            }
        }
        public static string EscapeCsv(string value)
        {
            return (value.IndexOfAny(new[] { ',', '\r', '\n', '"' }) >= 0 ? ('"' + value.Replace("\"", "\"\"") + '"') : value);
        }
    }
}
