﻿using Microsoft.EntityFrameworkCore;

namespace ExportCsvSample.Entity
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext(DbContextOptions<DatabaseContext> options)
            : base(options)
        {

        }
    }
}